<?php

/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/general.php
 */

return array(
    '*' => array(
        'siteUrl' => null,
        'defaultWeekStartDay' => 0,
        'enableCsrfProtection' => true,
        'omitScriptNameInUrls' => true,
        'cpTrigger' => 'admin',
    ),

    'assembla.ge' => array(
        'devMode' => true,
        'environmentVariables' => array(
            'basePath' => '/Users/GraphicActivity/Sites/assembla.ge/public/',
            'baseUrl'  => 'http://assembla.ge/',
        )
    ),

    'asmblg.co' => array(
        'devMode' => false,
        'environmentVariables' => array(
            'basePath' => '/var/www/html/',
            'baseUrl'  => 'https://asmblg.co/',
        )
    )
);