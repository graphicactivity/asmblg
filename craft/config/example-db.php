<?php

/**
 * Database Configuration
 *
 * All of your system's database configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/db.php
 */

return array(
    '*' => array(
        'tablePrefix' => 'craft',
        'server' => 'localhost',
    ),
    'assembla.ge' => array(
    	'database' => 'database',
        'user' => 'user',
        'password' => 'password',
    ),
    'asmblg.co' => array(
    	'database' => 'database',
        'user' => 'user',
        'password' => 'password',
    ),
);